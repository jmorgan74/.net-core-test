
using System;

namespace birdi_api.Models {

    public class Sighting {

        public long Id { get; set; }
        public string Name { get; set; }
        public DateTime SeenAt { get; set; }
        public string Note { get; set; }
        
    }

}