﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using birdi_api.Models;
using birdi_api.Repositories;

namespace birdi_api.Controllers
{

    [Route("api/[controller]")]
    public class SightingsController : Controller
    {   
        private IRepository<Sighting> _sightingRepository;

        public SightingsController(IRepository<Sighting> sightingsRepository) 
        {
            _sightingRepository = sightingsRepository;
        }

        // GET api/sightings
        [HttpGet]
        public IEnumerable<Sighting> Get()
        {
            return _sightingRepository.FindAll();
        }

        // GET api/sightings/5
        [HttpGet("{id}")]
        public Sighting Get(int id)
        {
            return _sightingRepository.FindBy(id);
        }

        // POST api/sightings
        [HttpPost]
        public void Post([FromBody]Sighting sighting)
        {
            _sightingRepository.Create(sighting);
        }

        // PUT api/sightings/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]Sighting sighting)
        {
        }

        // DELETE api/sightings/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
