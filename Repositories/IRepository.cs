using System.Collections.Generic;
using birdi_api.Models;

namespace birdi_api.Repositories
{

    public interface IRepository<T>
    {
        IEnumerable<T> FindAll();
        T FindBy(long id);
        long Create(Sighting sighting);
    }

}    