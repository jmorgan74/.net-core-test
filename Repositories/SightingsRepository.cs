using System;
using System.Collections.Generic;
using System.Linq;
using birdi_api.Repositories;
using birdi_api.Models;

namespace birdi_api.Repositories
{

    public class SightingsRepository : IRepository<Sighting>
    {
        private List<Sighting> _sightings = new List<Sighting>();

        public IEnumerable<Sighting> FindAll()
        {
            return _sightings;    
        }    

        public Sighting FindBy(long id)
        {
            return _sightings.Where(sighting => sighting.Id == id).FirstOrDefault();
        }

        public long Create(Sighting sighting) {

            long id = 3L;

            sighting.Id = id;

            _sightings.Add(sighting);

            return id;

        }

    }

}    